package messaging;

public class main {

	public static void main(String[] args) {
		System.out.println("Messaging");
		
		Device pi1 = new RPi("192.168.1.12", 5556);
		
		String message = pi1.listen();
		
		System.out.println(message);

	}

}
