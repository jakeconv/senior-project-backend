package messaging;
import java.nio.charset.StandardCharsets;

import org.zeromq.ZMQ;

public class RPi implements Device{
	
	private int port;
	private String ip_addr;
	private String address;

	public String listen() {
		String message = new String();
		byte[] input;
		System.out.println("Connecting to a Raspberry Pi device at " + address);
		ZMQ.Context context = ZMQ.context(1);
		ZMQ.Socket subscriber = context.socket(ZMQ.SUB);
		subscriber.connect(address);
		subscriber.subscribe("".getBytes(ZMQ.CHARSET)); //Subscribes to all messages from the Pi
		input = subscriber.recv(0);
		message = new String(input, StandardCharsets.UTF_8);  //Takes in the message, converts it to a String
		System.out.println("Recieved: " + message);
		subscriber.close(); //Closes the connection
		context.term();
		return message;
	}
	
	public RPi (String IP, int port_in){
		ip_addr = IP;
		port = port_in;
		formatAddr();
	}
	
	private void formatAddr(){
		address = "tcp://" + ip_addr + ":" + port;
	}
	
	public void changeIP(String newIP){
		ip_addr = newIP;
		formatAddr();
	}
	
	public void changePort(int newPort){
		port = newPort;
		formatAddr();
	}

}
