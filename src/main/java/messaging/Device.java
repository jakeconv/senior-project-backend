package messaging;

public interface Device {

	//Interface for listening to devices.  Intended to provide functionality for RPi and Arduino devices.
	
	String listen();
	
	
}
